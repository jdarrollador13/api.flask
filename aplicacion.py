from flask import Flask, jsonify
from flask_cors import CORS, cross_origin
from flask import request
import requests

app = Flask(__name__)
app.config['CORS_HEADERS'] = 'Content-Type'
#cors = CORS(app, resources={r"/api/v1/": {"origins": "*"}})
CORS(app, support_credentials=True)

#cors = CORS(app, resources={r"/foo": {"origins": "http://localhost:port"}})
@app.route('/api/v1/listar/personas', methods=['POST','OPTIONS'])
@cross_origin(supports_credentials=True)
#@cross_origin(origin='localhost',headers=['Content- Type','Authorization'])
#@cross_origin(origin='*',headers=['Content-Type','Authorization'])
def listaPersonas():
	data = request.get_json()
	res = requests.post('http://localhost:3001/api/v1/listar/personas',data)
	jsonResponse = res.json()
	dataJSON = {
		'status' : 'OK',
		'orden' : 'No_encontrada',
		'data' : []
	}
	return  jsonify(jsonResponse)

if (__name__ == '__main__'):
	app.run(debug=True, port=4000)